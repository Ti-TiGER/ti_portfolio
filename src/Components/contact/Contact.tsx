import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
import "./contact.css";

import { HiOutlineMail } from "react-icons/hi";

import { RiMessengerLine } from "react-icons/ri";

const contact = () => {
  const form = useRef<HTMLFormElement>(null);

  const sendEmail = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    emailjs.sendForm(
      "service_mofmaw5",
      "template_wtww88h",
      e.currentTarget,
      "bCCNGr8ADtFlKGdj7"
    );
    e.currentTarget.reset();
  };

  return (
    <section id="contact" className="Contact">
      <h5>Get in touch</h5>
      <h2>Contact Me</h2>

      <div className="container contact__container">
        <div className="contact__options">
          <article className="contact__option">
            <HiOutlineMail className="contact__option-icon" />
            <h4>Email</h4>
            <h5>tigerforwork1144@gmail.com</h5>
            <a href="mailto:tigerforwork1144@gmail.com" target="_blank">
              Send a message
            </a>
          </article>
          <article className="contact__option">
            <RiMessengerLine className="contact__option-icon" />
            <h4>Message</h4>
            <h5>Messenger Contact</h5>
            <a href="https://m.me/chairat.leksrisawat" target="_blank">
              Send a message
            </a>
          </article>
        </div>
        {/* END OF CONTACT OPTION */}

        <form ref={form} onSubmit={sendEmail}>
          <input
            type="text"
            name="name"
            placeholder="Your Full Name"
            required
          />
          <input type="email" name="email" placeholder="Your Email" required />
          <textarea
            name="message"
            rows={7}
            placeholder="Your Message"
            required
          ></textarea>
          <button type="submit" className="btn btn-primary">
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default contact;
