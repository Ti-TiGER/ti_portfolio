import React from "react";
import "./portfolio.css";

import PORT1 from "../../assets/portfolio1.png";
import PORT2 from "../../assets/portfolio2.jpg";
import PORT3 from "../../assets/portfolio3.jpg";
import PORT4 from "../../assets/portfolio4.jpg";
import PORT5 from "../../assets/portfolio5.jpg";
import PORT6 from "../../assets/portfolio6.jpg";

const data = [
  {
    id: 1,
    image: PORT1,
    title: "Store House(CMS)",
    gitlab: "https://gitlab.com/Ti-TiGER/cms",
    demo: "https://cms-black.vercel.app/",
  },
  {
    id: 2,
    image: PORT2,
    title: "Umaru - Doma",
    gitlab: "https://gitlab.com/Ti-TiGER/cms",
    demo: "https://cms-black.vercel.app/",
  },
  {
    id: 3,
    image: PORT3,
    title: "JETT",
    gitlab: "https://gitlab.com/Ti-TiGER/cms",
    demo: "https://cms-black.vercel.app/",
  },
  {
    id: 4,
    image: PORT4,
    title: "SAGE",
    gitlab: "https://gitlab.com/Ti-TiGER/cms",
    demo: "https://cms-black.vercel.app/",
  },
  {
    id: 5,
    image: PORT5,
    title: "KILLJOY",
    gitlab: "https://gitlab.com/Ti-TiGER/cms",
    demo: "https://cms-black.vercel.app/",
  },
  {
    id: 6,
    image: PORT6,
    title: "RAZE",
    gitlab: "https://gitlab.com/Ti-TiGER/cms",
    demo: "https://cms-black.vercel.app/",
  },
];

const Portfolio = () => {
  return (
    <section id="portfolio" className="portfolio">
      <h5>My React Work</h5>
      <h2>Portfolio</h2>

      <div className="container portfolio__container">
        {data.map(({ id, image, title, gitlab, demo }) => {
          return (
            <article key={id} className="portfolio__item">
              <div className="portfolio__item-image">
                <img src={image} alt={title} />
              </div>
              <h3>{title}</h3>
              <div className="portfolio__item-cta">
                <a href={gitlab} className="btn" target="_blank">
                  Gitlab
                </a>
                <a href={demo} className="btn btn-primary" target="_blank">
                  Live Demo
                </a>
              </div>
            </article>
          );
        })}
      </div>
    </section>
  );
};

export default Portfolio;
