import React from "react";
import "./footer.css";

import { RiFacebookCircleLine } from "react-icons/ri";
import { RiInstagramLine } from "react-icons/ri";
import { RiGithubLine } from "react-icons/ri";
import { RiGitlabLine } from "react-icons/ri";

const Footer = () => {
  return (
    <footer>
      <a href="#" className="footer__logo">
        Chairat
      </a>
      <ul className="permalinks">
        <li>
          <a href="#">Home</a>
        </li>
        <li>
          <a href="#about">About</a>
        </li>
        <li>
          <a href="#experience">Experience</a>
        </li>
        <li>
          <a href="#services">Services</a>
        </li>
        <li>
          <a href="#portfolio">Portfolio</a>
        </li>
        <li>
          <a href="#testimonials">Testimonials</a>
        </li>
        <li>
          <a href="#contact">Contact</a>
        </li>
      </ul>
      <div className="footer__socials">
        <a href="https://www.facebook.com/chairat.leksrisawat">
          <RiFacebookCircleLine className="footer__socials-icon" />
        </a>
        <a href="https://www.instagram.com/ti_tigers/">
          <RiInstagramLine className="footer__socials-icon" />
        </a>
        <a href="https://github.com/Ti-TiGER">
          <RiGithubLine className="footer__socials-icon" />
        </a>
        <a href="https://gitlab.com/Ti-TiGER">
          <RiGitlabLine className="footer__socials-icon" />
        </a>
      </div>

      <div className="footer__copyright">
        <small>&copy; TiGER Chairat. All rights reserved.</small>
      </div>
    </footer>
  );
};

export default Footer;
