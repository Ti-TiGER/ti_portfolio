import React from "react";
import { BsInstagram } from "react-icons/bs";
import { BsGithub } from "react-icons/bs";
import { FaGitlab } from "react-icons/fa";

const HeaderSocial = () => {
  return (
    <div className="header__socials">
      <a href="https://www.instagram.com/ti_tigers/" target="_blank">
        <BsInstagram />
      </a>
      <a href="https://github.com/Ti-TiGER" target="_blank">
        <BsGithub />
      </a>
      <a href="https://gitlab.com/Ti-TiGER" target="_blank">
        <FaGitlab />
      </a>
    </div>
  );
};

export default HeaderSocial;
