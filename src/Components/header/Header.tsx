import React from "react";

import CTA from "./CTA";
import HerderSocials from "./HeaderSocials";
import ME2 from "../../assets/me2.gif";

import "./header.css";

const Header = () => {
  return (
    <header>
      <div className="container header__container">
        <h5>Hello I'm</h5>
        <h1>Chairat Leksrisawat</h1>
        <h5 className="text-light">Frontend Developer</h5>
        <CTA />
        <HerderSocials />

        <div className="me">
          <img src={ME2} alt="me" />
        </div>

        <a href="#contact" className="scroll__down">
          Scrool Down
        </a>
      </div>
    </header>
  );
};

export default Header;
