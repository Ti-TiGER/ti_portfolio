import React from "react";
import "./testimonial.css";
import AVTR1 from "../../assets/avatar1.jpg";
import AVTR2 from "../../assets/avatar2.png";
import AVTR3 from "../../assets/avatar3.jpg";
import AVTR4 from "../../assets/avatar4.png";

// import Swiper core and required modules
import { Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

const data = [
  {
    avatar: AVTR1,
    name: "Crypt Sui",
    review:
      "This site definitely  appeals to the average person because the layout" +
      "is so  simple  but very  VERY  effective.  It is a clean  site  with a" +
      "flawless  look, and someone  without any  technical  background  would" +
      "definitely  appreciate  it.  The layout makes anyone feel  comfortable" +
      "because it is so well done and clean  looking.  You feel as if you are" +
      "in good hands and you know that you will be able to find  anything you" +
      "need on this site.",
  },
  {
    avatar: AVTR2,
    name: "Mafia Sui",
    review:
      "This site would appeal to anyone, technical background or not, because" +
      "it is simple and very  effective.  The  washed-out  image tiled in the" +
      "background is very welcoming and would appeal to anyone.",
  },
  {
    avatar: AVTR3,
    name: "Moon Sontiroad",
    review:
      "The site would  appeal to the average  person  because  the colors are" +
      "warm and inviting.  When I first  entered the site I could tell that a" +
      "lot of thought had gone into  making it an  excellent  one- I can tell" +
      "that it has been worked on for a while and has been polished until the" +
      "creator  was  satisfied.  Someone  with no  technical  background  can" +
      "easily see all this.",
  },
  {
    avatar: AVTR4,
    name: "Supat Yutibun",
    review:
      "The layout makes you feel very  comfortable,  especially  the boxed-in" +
      "idea of the pages.  The soft and warm colors and  minimum  text on the" +
      "main  page are  great; a big  chunk of text on the  main  page  always" +
      "freaks the reader out, which is not what you want.",
  },
];

const Testimonial = () => {
  return (
    <section id="testimonial">
      <h5>Review from clients</h5>
      <h2>Testimonials</h2>

      <Swiper
        className="container testimonial__container"
        // install Swiper modules
        modules={[Pagination]}
        spaceBetween={40}
        slidesPerView={1}
        pagination={{ clickable: true }}
      >
        {data.map(({ avatar, name, review }, index) => {
          return (
            <SwiperSlide key={index} className="testimonial">
              <div className="client__avatar">
                <img src={avatar} alt="Alvatar One" />
              </div>
              <h5 className="client__name">{name}</h5>
              <small className="client__review">{review}</small>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </section>
  );
};

export default Testimonial;
