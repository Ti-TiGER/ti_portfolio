import React from "react";
import "./about.css";

import { RiAwardFill } from "react-icons/ri";
import { RiUserStarLine } from "react-icons/ri";
import { RiFolder2Line } from "react-icons/ri";

import ME from "../../assets/me but og.png";

const About = () => {
  return (
    <section id="about" className="about">
      <h5>Get To Know</h5>
      <h2>About Me</h2>

      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={ME} alt="About Image" />
          </div>
        </div>
        <div className="about__content">
          <div className="about__cards">
            <article className="about__card">
              <RiAwardFill className="about__icon" />
              <h5>Experience</h5>
              <small>Internship in Doodeep.co</small>
            </article>

            <article className="about__card">
              <RiUserStarLine className="about__icon" />
              <h5>Clients</h5>
              <small>Internship in Doodeep.co</small>
            </article>

            <article className="about__card">
              <RiFolder2Line className="about__icon" />
              <h5>Projects</h5>
              <small>2+ Complete Project</small>
            </article>
          </div>

          <p>
            This is my personal portfolio website to showcase my work and
            accomplishments to potential employers, clients, or collaborators.
          </p>

          <a href="#contact" className="btn btn-primary">
            Let's Talk
          </a>
        </div>
      </div>
    </section>
  );
};

export default About;
